const assert = require('assert')
const { Item, updateQuality } = require('../src/classes')
const ITEM_NAMES = ['Backstage passes', 'Aged Brie', 'Sulfuras', 'foo']

describe('Gilded Rose Tests', function () {
	it('should throw error if items passed to updateQuality is not type array', () => {
		try {
			updateQuality('notArray')
			assert.ok(false)
		} catch (error) {
			assert.strictEqual(error.message, 'updateQuality parameter `items` must be an array')
		}
	})

	it('should create new item and decrease its sellIn', () => {
		const item = new Item({ name: 'random item', sellIn: 0, quality: 0 })
		const updatedItems = updateQuality([item])

		assert.strictEqual(updatedItems.length, 1)
		assert.strictEqual(updatedItems[0].name, 'random item')
		assert.strictEqual(updatedItems[0].sellIn, -1)
		assert.strictEqual(updatedItems[0].quality, 0)
	})

	it('should improve quality for backstage passes by two when less than 11 days but more than 5', () => {
		const item = new Item({ name: 'Backstage passes', sellIn: 10, quality: 25 })
		const updatedItems = updateQuality([item])

		assert.strictEqual(updatedItems.length, 1)
		assert.strictEqual(updatedItems[0].name, 'Backstage passes')
		assert.strictEqual(updatedItems[0].quality, 27)
		assert.strictEqual(updatedItems[0].sellIn, 9)
	})

	it('should improve quality for backstage passes by three when less than 5 days but more than -1', () => {
		const item = new Item({ name: 'Backstage passes', sellIn: 4, quality: 25 })
		const updatedItems = updateQuality([item])

		assert.strictEqual(updatedItems.length, 1)
		assert.strictEqual(updatedItems[0].name, 'Backstage passes')
		assert.strictEqual(updatedItems[0].quality, 28)
		assert.strictEqual(updatedItems[0].sellIn, 3)
	})

	it('should set quality to 0 for backstage when concert ends', () => {
		const item = new Item({ name: 'Backstage passes', sellIn: 0, quality: 25 })
		const updatedItems = updateQuality([item])

		assert.strictEqual(updatedItems.length, 1)
		assert.strictEqual(updatedItems[0].name, 'Backstage passes')
		assert.strictEqual(updatedItems[0].quality, 0)
		assert.strictEqual(updatedItems[0].sellIn, -1)
	})

	it('should not change sellIn and quality for Sulfuras item', () => {
		const item = new Item({ name: 'Sulfuras, Hand of Ragnaros', sellIn: 5, quality: 80 })
		const updatedItems = updateQuality([item])

		assert.strictEqual(updatedItems.length, 1)
		assert.strictEqual(updatedItems[0].name, 'Sulfuras, Hand of Ragnaros')
		assert.strictEqual(updatedItems[0].sellIn, 5)
		assert.strictEqual(updatedItems[0].quality, 80)
	})

	it('should decrease sellIn for all items that ar not sulfuras', () => {
		const items = ITEM_NAMES.map(name => new Item({ name, quality: 40, sellIn: 5 }))
		const updatedItems = updateQuality(items)

		assert.strictEqual(updatedItems.length, 4)
		assert.ok(updatedItems.every(item => (item.sellIn === 4 || item.name.includes('Sulfuras'))))
	})

	it('should decrease quality twice as fast for conjured item', () => {
		const item = new Item({ name: 'Conjured', sellIn: 4, quality: 25 })
		const updatedItems = updateQuality([item])

		assert.strictEqual(updatedItems.length, 1)
		assert.strictEqual(updatedItems[0].name, 'Conjured')
		assert.strictEqual(updatedItems[0].quality, 23)
		assert.strictEqual(updatedItems[0].sellIn, 3)
	})
})