const { Validator } = require('jsonschema')
const validator = new Validator()

const validateJSON = (object, validationSchema) => {
	const validationResult = validator.validate(object, validationSchema)
	if (validationResult.errors.length) {
		throw new Error(`Missing values: ${validationResult.errors.join(', ')}`)
	}
}

module.exports = { validateJSON }