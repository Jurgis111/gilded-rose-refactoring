const { validateJSON } = require('../util')
const QUALITY_RANGE = [0, 50]

/*
	Changed this method to update items depending on their level
	I grouped items by my own logic from less logic required to most logic required items
	Sulfuras always goes first as it never changes
	then all items decreases their sellIn, because they gets older
	after that follows Aged Brie, Backstage passes or Conjured independent quality changes.
	If none of above items were discovered, quality decreases by basic logic -> -1
	To decrease unhandled errors posibility added check if passed items are array
	and created validation for each item, that runs by itemValidationSchema declared below
*/
const updateQuality = (items = []) => {
	if (!Array.isArray(items)) { throw new Error('updateQuality parameter `items` must be an array') }

	for (const item of items) {
		validateJSON(item, itemValidationSchema)

		if (item.name.includes('Sulfuras')) { continue }

		item.sellIn = item.sellIn < 0 ? -1 : item.sellIn - 1

		if (item.name.includes('Aged Brie')) {
			item.quality = item.quality < QUALITY_RANGE[1] ? item.quality + 1 : QUALITY_RANGE[1]
			continue
		}

		if (item.name.includes('Backstage passes')) {
			item.quality = calculateBackstagePassQuality(item)
			continue
		}

		if (item.name.includes('Conjured')) {
			item.quality = item.quality > (QUALITY_RANGE[0] + 1) ? item.quality - 2 : QUALITY_RANGE[0]
			continue
		}

		item.quality = item.quality !== QUALITY_RANGE[0] ? item.quality - 1 : QUALITY_RANGE[0]
	}

	return items
}

const calculateBackstagePassQuality = (pass) => {
	if (pass.sellIn === -1) { return 0 }
	const newPassQuality = pass.sellIn < 5 ? pass.quality + 3 : pass.quality + 2

	return newPassQuality > QUALITY_RANGE[1] ? 50 : newPassQuality
}

const itemValidationSchema = {
	type: 'object',
	properties: {
		sellIn: { type: 'number', minValue: -1, maxValue: 11 },
		quality: { type: 'number', minValue: 0 },
		name: { type: 'string' }
	}
}

module.exports = {
	updateQuality,
}
