# Gilded Rose Refactoring

## Installation

Use following command to install project

```
npm install
```

## Program

To run program correctly, `generatedInput` file must be created. You can use `node fileCreationScript.js` command for that.
When `generatedInput` exists, run following command to execute script
```
npm start
```
This command starts index.js script, that reads data from `genereatedInput` using streams, transforms it using gildedRose code and writes to files
in `/ouput` directory. Files in `/output` directory are separated by gildedRose item types, or if item type was not found, they will be written to
`/allOther` file.

## Testing

To run tests run following command in terminal

```
npm run test
```

For test coverage run following command in terminal. Report will be generated in `/coverage` folder

```
npm run test:coverage
```

## Items generating

To generate new items for Gilded Rose, run following script using node command. Files will be generated into `generatedInput` file
```
node .\fileCreationScript.js
```

Initial gilded rose code is taken from this [repository](https://github.com/emilybache/GildedRose-Refactoring-Kata/tree/master/js-mocha)