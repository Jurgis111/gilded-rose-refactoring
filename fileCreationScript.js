const fs = require('fs')
const { Readable } = require('stream')

const OUPUT_DESTINATION = './generatedInput'
const ITEM_NAMES = ['Backstage passes', 'Aged Brie', 'Sulfuras', 'Conjured', 'foo']
const QUALITY_INTERVAL = [0, 50]
const SELLINS_INTERVAL = [-1, 10]
const MAX_ITEM_COUNT = 100000000

class InputGenerator extends Readable {
	constructor(options) {
		super(options)

		this.count = 0
		this.moreData = true
	}

	_read() {
		let mayPush = true
		do {
			mayPush = this.push(generateInputLine())
			if (this.count++ >= MAX_ITEM_COUNT) {
				this.moreData = false
			}
		} while (mayPush && this.moreData)

		if (mayPush) {
			console.log('finish')
			this.push(null)
		}
	}
}

const inputGenerator = new InputGenerator()
const output = fs.createWriteStream(OUPUT_DESTINATION)

inputGenerator.pipe(output)
function generateInputLine() {
	return `${ITEM_NAMES[random(0, 4)]}#${random(QUALITY_INTERVAL[0], QUALITY_INTERVAL[1])}#${random(SELLINS_INTERVAL[0], SELLINS_INTERVAL[1])}\n`
}
function random(min, max) {
	return Math.floor(Math.random() * (max - min)) + min
}