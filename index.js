const stream = require('stream')
const fs = require('fs')
const { updateQuality } = require('./js-mocha/src/classes')

const DATA_FILE = `${process.cwd()}/generatedInput`
const ENCODING = 'utf8'

class Transform extends stream.Transform {
	constructor(props) {
		super(props)
	}

	_transform(chunk, enc, done) {
		const items = updateQuality(
			chunk.toString('utf8').split('\n').map(line => {
				const [name, quality, sellIn] = line.split('#')
				return {
					name: name || 'foo',
					quality: Number(quality) || 0,
					sellIn: Number(sellIn) || 0,
				}
			})
		)
		const output = items.map(item => Object.values(item).join('#')).join('\n')
		done(null, output)
	}
}

const transform = new Transform()
transform.setEncoding('utf8')
const readStream = fs.createReadStream(DATA_FILE, ENCODING)
const defaultWriteStream = fs.createWriteStream(`${process.cwd()}/outputs/allOther`, ENCODING)

const writeStreamsByItem = {
	'Sulfuras': fs.createWriteStream(`${process.cwd()}/outputs/sulfuras`, ENCODING),
	'Aged Brie': fs.createWriteStream(`${process.cwd()}/outputs/agedBrie`, ENCODING),
	'Backstage passes': fs.createWriteStream(`${process.cwd()}/outputs/backstagePasses`, ENCODING),
	'Conjured': fs.createWriteStream(`${process.cwd()}/outputs/conjured`, ENCODING),
}

let processedCount = 0

/*
tried to achieve same thing below using promisified stream.pipeline
but could not achieve separating writeStream files.
Also node v14 has fs.promises, that could be used in pipe, that
might be considered in using
*/
readStream
	.pipe(transform)
	.on('data', (chunk) => {
		const lines = chunk.split('\n')
		for (const line of lines) {
			const writableStream = writeStreamsByItem[line.split('#')[0]] || defaultWriteStream
			writableStream.write(`${line}\n`)
		}
		processedCount += lines.length
		process.stdout.clearLine()
		process.stdout.cursorTo(0)
		process.stdout.write(`processed ${processedCount} items`)
	})
	.on('error', (err) => {
		console.log('error while reading file.', err)
	})